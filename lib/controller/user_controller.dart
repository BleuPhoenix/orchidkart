import 'package:OrchidKart/OrchidKart.dart';
import 'package:aqueduct/aqueduct.dart';

class UserController extends ResourceController {
  final _users = [
    {
      "id": 0,
      "details": {"name": "Test 1", "age": "20"}
    },
    {
      "id": 1,
      "details": {"name": "Test 2", "age": "25"}
    },
    {
      "id": 2,
      "details": {"name": "Test 3", "age": "30"}
    }
  ];

  @Operation.get()
  Future<Response> getAllUsers() async {
    return Response.ok(_users);
  }

  @Operation.get('id')
  Future<Response> getUserByID(@Bind.path('id') int id) async {
    final id = int.parse(request.path.variables['id']);

    final user =
        _users.firstWhere((user) => user['id'] == id, orElse: () => null);
    if (user == null) {
      return Response.notFound();
    }

    return Response.ok(user);
  }
}
